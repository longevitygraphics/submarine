<?php
/**
 * Template Name: Job Opportunities
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<section class="content">
		<main id="content" role="main">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop', 'page' );
			?>

<hr/>

<h3><img src="/wp-content/uploads/2017/05/DSC00717-1-300x225.jpeg" alt="Job Opportunities" width="300" height="225" class="alignright size-medium wp-image-140" />Current Openings</h3>
<?php
$numposts = 0;
query_posts("cat=5&orderby=date&order=DESC");
if(have_posts()) :
while(have_posts()) :the_post();
$numposts++;
?>
<?php the_time('F Y'); ?><br/>
<a class="pdf" href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br/>
<br/>
<?php
endwhile;
endif;
wp_reset_query();?>

<?php
if($numposts == 0){
    echo '<i>We currently do not have any Job Openings</i>';
}
?>
<div style="clear:both;"></div>

		</main>

<?php get_sidebar(); ?>
</section>

<?php get_footer(); ?>

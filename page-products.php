<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

function terms_sort($a, $b)
{
	$order_a = get_field('order', $a);
	$order_b = get_field('order', $b);
    return $order_a - $order_b;
}

get_header(); ?>

<section class="content">
		<main id="content" role="main">
			
		<h2><?php the_title(); ?></h2>
		<?php the_content(); ?>


		<?php 
			$terms = get_terms('product-category');
		?>

		<?php if(sizeof($terms) > 0): 
		usort($terms, "terms_sort");
		?>
			<div class="category-list">
				<?php foreach ($terms as $key => $value): 
				wp_reset_query();
				$image = get_field('category_image', $value);
				?>
					<div>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
						<div class="name"><?php echo $value->name; ?></div>
						<?php 
							$args = array(
							    'post_type' => 'product',
							    'post_status' => 'publish',
							    'tax_query' => array(
							        array (
							            'taxonomy' => 'product-category',
							            'field' => 'slug',
							            'terms' => $value->slug,
							        )
							    ),
							);
					        $result = new WP_Query( $args );

					        // Loop
					        if ( $result->have_posts() ) :
					        	?>
								<ul class="products-list">
					        	<?php
					            while( $result->have_posts() ) : $result->the_post();?>
					        		<li><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></li>
								<?php
					            endwhile;
					            ?>
								</ul>
					            <?php
					        endif; // End Loop

					        wp_reset_query();
					    ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>

		</main>

<?php get_sidebar(); ?>
</section>

<?php get_footer(); ?>

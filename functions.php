<?php

function child_twentyten_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Sidebar Area', 'twentyten' ),
		'id' => 'primary-widget-area',
		'description' => __( 'Add widgets here to appear in your sidebar.', 'twentyten' ),
		'before_widget' => '<section id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Area', 'twentyten' ),
		'id' => 'footer-widget-area',
		'description' => __( 'These widgets will show up on your footer', 'twentyten' ),
		'before_widget' => '<section id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
}

function lg_enqueue_styles_scripts() {
    
  wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css', false );
  wp_enqueue_style( 'slickcss', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css', false );
  wp_enqueue_style('slickthemecss', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css', false);
  wp_enqueue_style( 'longevity-css', get_stylesheet_directory_uri() . '/css/longevity.css', '1.0' );

  wp_register_script( 'slickjs', "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js", [], '1.0.0', true );

  wp_enqueue_script( 'slickjs' );

}
add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );
//AFTER THEME IS SET UP (FUNCTIONS.PHP IS LOADED FROM PARENT THEME) - REMOVE THE SIDEBAR WIDGETS
add_action('after_setup_theme','remove_parent_widgets');
function remove_parent_widgets() {
    remove_action( 'widgets_init', 'twentyten_widgets_init' );
}
//RE-ADD THE SIDEBAR WIDGETS WITH THE NEW CODE ADDED, DUPLICATING THE FUNCTION
add_action( 'after_setup_theme', 'child_twentyten_widgets_init' );

//ADD REALLY SIMPLE CAPTCHA BACK TO CONTACT FORM 7
add_filter( 'wpcf7_use_really_simple_captcha', '__return_true' );

//CHANGE DEFAULT THEME NAME
add_filter('default_page_template_title', function() {
    return __('One column, right sidebar', 'your_text_domain');
});

//REMOVES HENTRY FROM PAGES TO ELIMINATE ERRORS IN GOOGLE WEBMASTER TOOLS
function themeslug_remove_hentry( $classes ) {
    if ( is_page() ) {
        $classes = array_diff( $classes, array( 'hentry' ) );
    }
    return $classes;
}
add_filter( 'post_class','themeslug_remove_hentry' );

//REMOVES QUERY(?) STRINGS FROM URLS
function _remove_script_version( $src ){
	$parts = explode( '?ver', $src );
	return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

//DEFER JQUERY PARSING USING THE HTML5 DEFER PROPERTY
if (!(is_admin() )) {
    function defer_parsing_of_js ( $url ) {
        if ( FALSE === strpos( $url, '.js' ) ) return $url;
        if ( strpos( $url, 'jquery.js' ) ) return $url;
        // return "$url' defer ";
        return "$url' defer onload='";
    }
    add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );
}

//DO NOT LOAD CONTACT FORM 7 JS/CSS ON PAGES THAT DON'T HAVE FORMS
function conditionally_load_contactform7(){
    if(is_page(array( 'homepage'))) { 
	wp_dequeue_script('contact-form-7'); 
	wp_dequeue_style('contact-form-7'); 	
    }
}	
add_action( 'wp_enqueue_scripts', 'conditionally_load_contactform7' );

//LOAD JQUERY SMOOTHSCROLL.JS
wp_enqueue_script( 'smoothup', get_stylesheet_directory_uri() . '/js/smoothscroll.js', array( 'jquery' ), '',  true );

//REMOVES THE JOB OPPORTUNITIES CATEGORY FROM SHOWING ON THE NEWS PAGE
function exclude_category($query) {
  if ( $query->is_home() ) {
    $query->set('cat', '-5');
  }
    return $query;
  }
add_filter('pre_get_posts', 'exclude_category');

//REMOVE THE 'CONTINUE READING' TEXT FROM THE BLOG
function modify_read_more_link() {
    return '';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );

//ALLOWS YOU TO EXECUTE PHP IN WIDGETS
add_filter('widget_text','execute_php',100);
function execute_php($html){
     if(strpos($html,"<"."?php")!==false){
          ob_start();
          eval("?".">".$html);
          $html=ob_get_contents();
          ob_end_clean();
     }
     return $html;
}

//Taxonomy
function create_taxonomy(){
  //Blog Category
  register_taxonomy(
    'product-category',
    'product',
    array(
      'label' => __( 'Category' ),
      'rewrite' => array( 'slug' => 'product-category' ),
      'hierarchical' => true,
    )
  );
}

add_action( 'init', 'create_taxonomy' );

//Custom Post Types
function create_post_type() {

    // PRODUCTS
    register_post_type( 'product',
        array(
            'labels' => array(
                'name'          => __( 'Products' ),
                'singular_name' => __( 'Product' )
            ),
            'public'      => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-cart',
            'supports' => array( 'thumbnail','title', 'editor', 'excerpt', 'revisions' ),
        )
    );

}
add_action( 'init', 'create_post_type' );

function ise_get_archives( $args = '' ) {
    global $wpdb, $wp_locale;
 
    $defaults = array(
        'type'            => 'monthly',
        'limit'           => '',
        'format'          => 'html',
        'before'          => '',
        'after'           => '',
        'show_post_count' => false,
        'echo'            => 1,
        'order'           => 'DESC',
        'post_type'       => 'post',
		'category'        => NULL,
        'year'            => get_query_var( 'year' ),
        'monthnum'        => get_query_var( 'monthnum' ),
        'day'             => get_query_var( 'day' ),
        'w'               => get_query_var( 'w' ),
    );
 
    $parsed_args = wp_parse_args( $args, $defaults );
 
    $post_type_object = get_post_type_object( $parsed_args['post_type'] );
    if ( ! is_post_type_viewable( $post_type_object ) ) {
        return;
    }
 
    $parsed_args['post_type'] = $post_type_object->name;
 
    if ( '' === $parsed_args['type'] ) {
        $parsed_args['type'] = 'monthly';
    }
 
    if ( ! empty( $parsed_args['limit'] ) ) {
        $parsed_args['limit'] = absint( $parsed_args['limit'] );
        $parsed_args['limit'] = ' LIMIT ' . $parsed_args['limit'];
    }
 
    $order = strtoupper( $parsed_args['order'] );
    if ( 'ASC' !== $order ) {
        $order = 'DESC';
    }
 
    // This is what will separate dates on weekly archive links.
    $archive_week_separator = '&#8211;';
 
	$where_query = "WHERE post_type = %s AND post_status = 'publish' AND p.id = tr.object_id AND t.term_id = tt.term_id AND tr.term_taxonomy_id = tt.term_taxonomy_id";
	
	if( ! is_null($parsed_args['category']) ) {
		$where_query .= " AND (tt.taxonomy = 'category' AND tt.term_id = t.term_id AND tt.term_id = " . $parsed_args['category'] . ")";
    }
	
    $sql_where = $wpdb->prepare( $where_query, $parsed_args['post_type'] );
 
    /**
     * Filters the SQL WHERE clause for retrieving archives.
     *
     * @since 2.2.0
     *
     * @param string $sql_where   Portion of SQL query containing the WHERE clause.
     * @param array  $parsed_args An array of default arguments.
     */
    $where = apply_filters( 'getarchives_where', $sql_where, $parsed_args );
 
    /**
     * Filters the SQL JOIN clause for retrieving archives.
     *
     * @since 2.2.0
     *
     * @param string $sql_join    Portion of SQL query containing JOIN clause.
     * @param array  $parsed_args An array of default arguments.
     */
    $join = apply_filters( 'getarchives_join', '', $parsed_args );
 
    $output = '';
 
    $last_changed = wp_cache_get_last_changed( 'posts' );
 
    $limit = $parsed_args['limit'];
 
    if ( 'monthly' === $parsed_args['type'] ) {
        $query   = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, count(ID) as posts FROM $join $where GROUP BY YEAR(post_date), MONTH(post_date) ORDER BY post_date $order $limit";
        $key     = md5( $query );
        $key     = "wp_get_archives:$key:$last_changed";
        $results = wp_cache_get( $key, 'posts' );
        if ( ! $results ) {
            $results = $wpdb->get_results( $query );
            wp_cache_set( $key, $results, 'posts' );
        }
        if ( $results ) {
            $after = $parsed_args['after'];
            foreach ( (array) $results as $result ) {
                $url = get_month_link( $result->year, $result->month );
                if ( 'post' !== $parsed_args['post_type'] ) {
                    $url = add_query_arg( 'post_type', $parsed_args['post_type'], $url );
                }
                /* translators: 1: Month name, 2: 4-digit year. */
                $text = sprintf( __( '%1$s %2$d' ), $wp_locale->get_month( $result->month ), $result->year );
                if ( $parsed_args['show_post_count'] ) {
                    $parsed_args['after'] = '&nbsp;(' . $result->posts . ')' . $after;
                }
                $selected = is_archive() && (string) $parsed_args['year'] === $result->year && (string) $parsed_args['monthnum'] === $result->month;
                $output  .= get_archives_link( $url, $text, $parsed_args['format'], $parsed_args['before'], $parsed_args['after'], $selected );
            }
        }
    } elseif ( 'yearly' === $parsed_args['type'] ) {
        $query   = "SELECT YEAR(post_date) AS `year`, count(ID) as posts FROM $wpdb->posts p, $wpdb->terms t, $wpdb->term_relationships tr, $wpdb->term_taxonomy tt $join $where GROUP BY YEAR(post_date) ORDER BY post_date $order $limit";
        $key     = md5( $query );
        $key     = "wp_get_archives:$key:$last_changed";
        $results = wp_cache_get( $key, 'posts' );
        if ( ! $results ) {
            $results = $wpdb->get_results( $query );
            wp_cache_set( $key, $results, 'posts' );
        }
		
        if ( $results ) {
            $after = $parsed_args['after'];
            foreach ( (array) $results as $result ) {
                $url = get_year_link( $result->year );
                if ( 'post' !== $parsed_args['post_type'] ) {
                    $url = add_query_arg( 'post_type', $parsed_args['post_type'], $url );
                }
				
				if( ! is_null($parsed_args['category']) ) {
					$url .= '?cat=' . $parsed_args['category'];
			    }
				
                $text = sprintf( '%d', $result->year );
                if ( $parsed_args['show_post_count'] ) {
                    $parsed_args['after'] = '&nbsp;(' . $result->posts . ')' . $after;
                }
                $selected = is_archive() && (string) $parsed_args['year'] === $result->year;
                $output  .= get_archives_link( $url, $text, $parsed_args['format'], $parsed_args['before'], $parsed_args['after'], $selected );
            }
        }
    } elseif ( 'daily' === $parsed_args['type'] ) {
        $query   = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, DAYOFMONTH(post_date) AS `dayofmonth`, count(ID) as posts FROM $wpdb->posts $join $where GROUP BY YEAR(post_date), MONTH(post_date), DAYOFMONTH(post_date) ORDER BY post_date $order $limit";
        $key     = md5( $query );
        $key     = "wp_get_archives:$key:$last_changed";
        $results = wp_cache_get( $key, 'posts' );
        if ( ! $results ) {
            $results = $wpdb->get_results( $query );
            wp_cache_set( $key, $results, 'posts' );
        }
        if ( $results ) {
            $after = $parsed_args['after'];
            foreach ( (array) $results as $result ) {
                $url = get_day_link( $result->year, $result->month, $result->dayofmonth );
                if ( 'post' !== $parsed_args['post_type'] ) {
                    $url = add_query_arg( 'post_type', $parsed_args['post_type'], $url );
                }
                $date = sprintf( '%1$d-%2$02d-%3$02d 00:00:00', $result->year, $result->month, $result->dayofmonth );
                $text = mysql2date( get_option( 'date_format' ), $date );
                if ( $parsed_args['show_post_count'] ) {
                    $parsed_args['after'] = '&nbsp;(' . $result->posts . ')' . $after;
                }
                $selected = is_archive() && (string) $parsed_args['year'] === $result->year && (string) $parsed_args['monthnum'] === $result->month && (string) $parsed_args['day'] === $result->dayofmonth;
                $output  .= get_archives_link( $url, $text, $parsed_args['format'], $parsed_args['before'], $parsed_args['after'], $selected );
            }
        }
    } elseif ( 'weekly' === $parsed_args['type'] ) {
        $week    = _wp_mysql_week( '`post_date`' );
        $query   = "SELECT DISTINCT $week AS `week`, YEAR( `post_date` ) AS `yr`, DATE_FORMAT( `post_date`, '%Y-%m-%d' ) AS `yyyymmdd`, count( `ID` ) AS `posts` FROM `$wpdb->posts` $join $where GROUP BY $week, YEAR( `post_date` ) ORDER BY `post_date` $order $limit";
        $key     = md5( $query );
        $key     = "wp_get_archives:$key:$last_changed";
        $results = wp_cache_get( $key, 'posts' );
        if ( ! $results ) {
            $results = $wpdb->get_results( $query );
            wp_cache_set( $key, $results, 'posts' );
        }
        $arc_w_last = '';
        if ( $results ) {
            $after = $parsed_args['after'];
            foreach ( (array) $results as $result ) {
                if ( $result->week != $arc_w_last ) {
                    $arc_year       = $result->yr;
                    $arc_w_last     = $result->week;
                    $arc_week       = get_weekstartend( $result->yyyymmdd, get_option( 'start_of_week' ) );
                    $arc_week_start = date_i18n( get_option( 'date_format' ), $arc_week['start'] );
                    $arc_week_end   = date_i18n( get_option( 'date_format' ), $arc_week['end'] );
                    $url            = add_query_arg(
                        array(
                            'm' => $arc_year,
                            'w' => $result->week,
                        ),
                        home_url( '/' )
                    );
                    if ( 'post' !== $parsed_args['post_type'] ) {
                        $url = add_query_arg( 'post_type', $parsed_args['post_type'], $url );
                    }
                    $text = $arc_week_start . $archive_week_separator . $arc_week_end;
                    if ( $parsed_args['show_post_count'] ) {
                        $parsed_args['after'] = '&nbsp;(' . $result->posts . ')' . $after;
                    }
                    $selected = is_archive() && (string) $parsed_args['year'] === $result->yr && (string) $parsed_args['w'] === $result->week;
                    $output  .= get_archives_link( $url, $text, $parsed_args['format'], $parsed_args['before'], $parsed_args['after'], $selected );
                }
            }
        }
    } elseif ( ( 'postbypost' === $parsed_args['type'] ) || ( 'alpha' === $parsed_args['type'] ) ) {
        $orderby = ( 'alpha' === $parsed_args['type'] ) ? 'post_title ASC ' : 'post_date DESC, ID DESC ';
        $query   = "SELECT * FROM $wpdb->posts $join $where ORDER BY $orderby $limit";
        $key     = md5( $query );
        $key     = "wp_get_archives:$key:$last_changed";
        $results = wp_cache_get( $key, 'posts' );
        if ( ! $results ) {
            $results = $wpdb->get_results( $query );
            wp_cache_set( $key, $results, 'posts' );
        }
        if ( $results ) {
            foreach ( (array) $results as $result ) {
                if ( '0000-00-00 00:00:00' !== $result->post_date ) {
                    $url = get_permalink( $result );
					
					if( ! is_null($parsed_args['category']) ) {
						$url .= '?cat=' . $parsed_args['category'];
					}
					
                    if ( $result->post_title ) {
                        /** This filter is documented in wp-includes/post-template.php */
                        $text = strip_tags( apply_filters( 'the_title', $result->post_title, $result->ID ) );
                    } else {
                        $text = $result->ID;
                    }
                    $selected = get_the_ID() === $result->ID;
                    $output  .= get_archives_link( $url, $text, $parsed_args['format'], $parsed_args['before'], $parsed_args['after'], $selected );
                }
            }
        }
    }
 
    if ( $parsed_args['echo'] ) {
        echo $output;
    } else {
        return $output;
    }
}

?>
<?php
/**
 * Template Name: Fullwidth Template
 *
 * A custom page template without sidebar that stretches to either side of the screen.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<section class="content <?php if(is_front_page()){ echo " special";} ?> ">
		<main id="content" role="main" class="one-column">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			 get_template_part( 'loop', 'page' );
			?>

		</main>
</section>

<?php get_footer(); ?>

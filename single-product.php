<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<section class="content">
		<main role="main" class="single-product">

			<?php
			/* Run the loop to output the post.
			 * If you want to overload this in a child theme then include a file
			 * called loop-single.php and that will be used instead.
			 */
			$feature_image = get_field('feature_image');
			$content = get_field('content');
			$additional_content = get_field('additional_content');
			$pdf = get_field('pdf_group');
			$video = get_field('video');
			?>

			<?php if($feature_image): ?>
				<img src="<?php echo $feature_image['url']; ?>" alt="<?php echo $feature_image['alt']; ?>" class="full-image">
			<?php endif; ?>

			<div>
				<div>
					<h2 class="h5 title"><?php the_title(); ?></h2>

					<p><?php echo $content; ?></p>
					<a style="float:right; margin: 10px;" href="/our-company/contact/" class="product-cta">CONTACT US TODAY</a>
					<?php echo $additional_content; ?>
				</div>
			</div>

			
			<hr>

			<div class="product-media" style="max-width:100%;">
				<?php if($video): ?>
				<div class="video">
					<?php echo $video; ?>
				</div>
				<?php endif; ?>

				<div class="row">
					<div class="col-md-6 col-sm-12 data-sheet">
						<div class="feature-image" style="width: 100%">
							<img src="<?php echo $pdf['thumbnail']['url']; ?>" alt="">
							<?php if($pdf['thumbnail']['caption']): ?>
								<div class="caption"><?php echo $pdf['thumbnail']['caption']; ?></p></div>
							<?php endif; ?>
						</div>
						<?php if($pdf['pdf']): ?>
							<a href="<?php echo $pdf['pdf']['url']; ?>"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PRODUCT DOCUMENT INFORMATION</a>
						<?php endif; ?>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="gallery">
						<?php
							if( have_rows('gallery') ):
								?>
								<div class="gallery-carousel">
								<?php
							    while ( have_rows('gallery') ) : the_row();
							        $image = get_sub_field('image');
							        $caption = $image['caption'];
							        ?>
										<div>
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
											<?php if($caption): ?>
												<div class="caption"><?php echo $caption; ?></div>
											<?php endif; ?>
										</div>
							        <?php
							    endwhile;
							    ?>
								</div>
							    <?php
							else :
							    // no rows found
							endif;

							?>		
						</div>
					</div>
				</div>
			</div>

		</main>
</section>

<script>
	jQuery(document).ready(function($){
	  jQuery('.gallery-carousel').slick({
	      dots: false,
	      arrow:true,
	      speed: 300,
	      slidesToShow: 1,
	      adaptiveHeight: true,
	      autoplay: true
	  });
	});
</script>

<?php get_footer(); ?>
